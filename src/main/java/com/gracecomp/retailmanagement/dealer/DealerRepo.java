package com.gracecomp.retailmanagement.dealer;

import org.springframework.data.repository.CrudRepository;

public interface DealerRepo extends CrudRepository<Dealer, Long> {

}
