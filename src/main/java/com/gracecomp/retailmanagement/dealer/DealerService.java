package com.gracecomp.retailmanagement.dealer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DealerService {

    public interface IDealerService {

        List<Dealer> findAll();

        List<Dealer> findAllDirectDealer(Boolean isDirect);

        List<Dealer> findAllInDirectDealer(Boolean inDirect);
    }

    @Service
    public static class Services implements IDealerService {

        @Autowired
        private DealerRepo dealerRepo;

        @Override
        public List<Dealer> findAll() {
            List<Dealer> listDealer = new ArrayList<>();
            dealerRepo.findAll().forEach(listDealer::add);
            return listDealer;
        }

        @Override
        public List<Dealer> findAllDirectDealer(Boolean isDirect) {
            List<Dealer> listDealer = new ArrayList<>();
            dealerRepo.findAll().forEach(listDealer::add);
            return listDealer.stream().filter(it -> isDirect)
                    .collect(Collectors.toList());
        }

        @Override
        public List<Dealer> findAllInDirectDealer(Boolean inDirect) {
            List<Dealer> listDealer = new ArrayList<>();
            dealerRepo.findAll().forEach(listDealer::add);
            return listDealer.stream().filter(it -> inDirect)
                    .collect(Collectors.toList());
        }
    }

}

