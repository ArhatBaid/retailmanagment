package com.gracecomp.retailmanagement.dealer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

public class DealerController {

    @Autowired
    private DealerService.Services dealersService;

    @RequestMapping("/dealer")
    public List<Dealer> getAllDealers() {
        return dealersService.findAll();
    }
}
