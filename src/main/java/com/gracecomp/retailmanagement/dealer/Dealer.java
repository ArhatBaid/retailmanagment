package com.gracecomp.retailmanagement.dealer;

import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_DEALER)
public class Dealer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.DEALER_ID, nullable = false)
    private Long id;

    @Column(name = DatabaseUtils.DEALER_EMAIL, nullable = false)
    private String email;

    @Column(name = DatabaseUtils.DEALER_PASSWORD, nullable = false)
    private String password;

    @Column(name = DatabaseUtils.DEALER_FIRST_NAME, nullable = false)
    private String f_name;

    @Column(name = DatabaseUtils.DEALER_LAST_NAME, nullable = false)
    private String l_name;

    @Column(name = DatabaseUtils.DEALER_IS_DIRECT, nullable = false)
    private Boolean is_direct;

    @Column(name = DatabaseUtils.DEALER_CREDIT_LIMIT, nullable = false)
    private String credit_limit;

    @Column(name = DatabaseUtils.DEALER_AUTH_STATUS, nullable = false)
    private String auth_status;

    @Column(name = DatabaseUtils.DEALER_PHONE_NUMBER, nullable = false)
    private String phone_number;

    @Column(name = DatabaseUtils.DEALER_FAX)
    private String fax;

    @Column(name = DatabaseUtils.DEALER_SHIPPING_ADDRESS_LINE_1, nullable = false)
    private String ship_add_line_1;

    @Column(name = DatabaseUtils.DEALER_SHIPPING_ADDRESS_LINE_2)
    private String ship_add_line_2;

    @Column(name = DatabaseUtils.DEALER_SHIPPING_ADDRESS_CITY, nullable = false)
    private String ship_add_city;

    @Column(name = DatabaseUtils.DEALER_SHIPPING_ADDRESS_STATE, nullable = false)
    private String ship_add_state;

    @Column(name = DatabaseUtils.DEALER_SHIPPING_ADDRESS_COUNTRY, nullable = false)
    private String ship_add_country;

    @Column(name = DatabaseUtils.DEALER_SHIPPING_ADDRESS_ZIPCODE, nullable = false)
    private String ship_add_zipcode;

    @Column(name = DatabaseUtils.DEALER_BILLING_ADDRESS_LINE_1, nullable = false)
    private String bill_add_line_1;

    @Column(name = DatabaseUtils.DEALER_BILLING_ADDRESS_LINE_2)
    private String bill_add_line_2;

    @Column(name = DatabaseUtils.DEALER_BILLING_ADDRESS_CITY, nullable = false)
    private String bill_add_city;

    @Column(name = DatabaseUtils.DEALER_BILLING_ADDRESS_STATE, nullable = false)
    private String bill_add_state;

    @Column(name = DatabaseUtils.DEALER_BILLING_ADDRESS_COUNTRY, nullable = false)
    private String bill_add_country;

    @Column(name = DatabaseUtils.DEALER_BILLING_ADDRESS_ZIPCODE, nullable = false)
    private String bill_add_zipcode;
}
