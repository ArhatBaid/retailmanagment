package com.gracecomp.retailmanagement.bookdemo;

import org.springframework.data.repository.CrudRepository;

public interface BookCategoryRepository extends CrudRepository<BookCategory, Integer> {
}