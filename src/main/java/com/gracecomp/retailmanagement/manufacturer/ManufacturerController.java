package com.gracecomp.retailmanagement.manufacturer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
public class ManufacturerController {

    @Autowired
    private ManufacturerService.Services manufacturerService;

    @RequestMapping(EndPoints.MANUFACTURER)
    public List<Manufacturer> getAllManufacturers() {
        return manufacturerService.findAll();
    }

    static class EndPoints{
        private static final String MANUFACTURER = "manufacturer";
    }
}
