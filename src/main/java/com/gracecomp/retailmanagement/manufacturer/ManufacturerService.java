package com.gracecomp.retailmanagement.manufacturer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public class ManufacturerService {

    public interface IManufacturerService {

        List<Manufacturer> findAll();
    }

    @Service
    public static class Services implements IManufacturerService {

        private List<Manufacturer> listManufacturer = new ArrayList<>();

        @Autowired
        private ManufacturerRepo manufacturerRepo;

        @Override
        public List<Manufacturer> findAll() {
            List<Manufacturer> listManufacturer = new ArrayList<>();
            manufacturerRepo.findAll().forEach(listManufacturer::add);
            return listManufacturer;
        }
    }
}

