package com.gracecomp.retailmanagement.manufacturer;

import org.springframework.data.repository.CrudRepository;

public interface ManufacturerRepo extends CrudRepository<Manufacturer, Long> {

}
