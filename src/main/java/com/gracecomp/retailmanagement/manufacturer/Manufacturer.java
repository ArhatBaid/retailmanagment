package com.gracecomp.retailmanagement.manufacturer;

import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_MANUFACTURER)
public class Manufacturer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.MANUFACTURER_ID, nullable = false)
    private Long id;

    @Column(name = DatabaseUtils.MANUFACTURER_EMAIL, nullable = false)
    private String email;

    @Column(name = DatabaseUtils.MANUFACTURER_PASSWORD, nullable = false)
    private String password;

    @Column(name = DatabaseUtils.MANUFACTURER_FIRST_NAME, nullable = false)
    private String f_name;

    @Column(name = DatabaseUtils.MANUFACTURER_LAST_NAME, nullable = false)
    private String l_name;

    @Column(name = DatabaseUtils.MANUFACTURER_PHONE_NUMBER, nullable = false)
    private String phone_number;

    @Column(name = DatabaseUtils.MANUFACTURER_FAX)
    private String fax;

    @Column(name = DatabaseUtils.MANUFACTURER_SHIPPING_ADDRESS_LINE_1, nullable = false)
    private String ship_add_line_1;

    @Column(name = DatabaseUtils.MANUFACTURER_SHIPPING_ADDRESS_LINE_2)
    private String ship_add_line_2;

    @Column(name = DatabaseUtils.MANUFACTURER_SHIPPING_ADDRESS_CITY, nullable = false)
    private String ship_add_city;

    @Column(name = DatabaseUtils.MANUFACTURER_SHIPPING_ADDRESS_STATE, nullable = false)
    private String ship_add_state;

    @Column(name = DatabaseUtils.MANUFACTURER_SHIPPING_ADDRESS_COUNTRY, nullable = false)
    private String ship_add_country;

    @Column(name = DatabaseUtils.MANUFACTURER_SHIPPING_ADDRESS_ZIPCODE, nullable = false)
    private String ship_add_zipcode;

    @Column(name = DatabaseUtils.MANUFACTURER_BILLING_ADDRESS_LINE_1, nullable = false)
    private String bill_add_line_1;

    @Column(name = DatabaseUtils.MANUFACTURER_BILLING_ADDRESS_LINE_2)
    private String bill_add_line_2;

    @Column(name = DatabaseUtils.MANUFACTURER_BILLING_ADDRESS_CITY, nullable = false)
    private String bill_add_city;

    @Column(name = DatabaseUtils.MANUFACTURER_BILLING_ADDRESS_STATE, nullable = false)
    private String bill_add_state;

    @Column(name = DatabaseUtils.MANUFACTURER_BILLING_ADDRESS_COUNTRY, nullable = false)
    private String bill_add_country;

    @Column(name = DatabaseUtils.MANUFACTURER_BILLING_ADDRESS_ZIPCODE, nullable = false)
    private String bill_add_zipcode;

}
