package com.gracecomp.retailmanagement.utils;

public class DatabaseUtils {
    /*Manufacturer*/
    public static final String TABLE_MANUFACTURER = "Manufacturer";
    public static final String MANUFACTURER_ID = "id";
    public static final String MANUFACTURER_EMAIL = "email";
    public static final String MANUFACTURER_PASSWORD = "password";
    public static final String MANUFACTURER_FIRST_NAME = "f_name";
    public static final String MANUFACTURER_LAST_NAME = "l_name";
    public static final String MANUFACTURER_PHONE_NUMBER = "phone_number";
    public static final String MANUFACTURER_FAX = "fax";
    public static final String MANUFACTURER_SHIPPING_ADDRESS_LINE_1 = "ship_add_line_1";
    public static final String MANUFACTURER_SHIPPING_ADDRESS_LINE_2 = "ship_add_line_2";
    public static final String MANUFACTURER_SHIPPING_ADDRESS_CITY = "ship_add_city";
    public static final String MANUFACTURER_SHIPPING_ADDRESS_STATE = "ship_add_state";
    public static final String MANUFACTURER_SHIPPING_ADDRESS_COUNTRY = "ship_add_country";
    public static final String MANUFACTURER_SHIPPING_ADDRESS_ZIPCODE = "ship_add_zipcode";
    public static final String MANUFACTURER_BILLING_ADDRESS_LINE_1 = "bill_add_line_1";
    public static final String MANUFACTURER_BILLING_ADDRESS_LINE_2 = "bill_add_line_2";
    public static final String MANUFACTURER_BILLING_ADDRESS_CITY = "bill_add_city";
    public static final String MANUFACTURER_BILLING_ADDRESS_STATE = "bill_add_state";
    public static final String MANUFACTURER_BILLING_ADDRESS_COUNTRY = "bill_add_country";
    public static final String MANUFACTURER_BILLING_ADDRESS_ZIPCODE = "bill_add_zipcode";

    /*Distributor*/
    public static final String TABLE_DISTRIBUTOR = "Distributor";
    public static final String DISTRIBUTOR_ID = "id";
    public static final String DISTRIBUTOR_EMAIL = "email";
    public static final String DISTRIBUTOR_PASSWORD = "password";
    public static final String DISTRIBUTOR_FIRST_NAME = "f_name";
    public static final String DISTRIBUTOR_LAST_NAME = "l_name";
    public static final String DISTRIBUTOR_CREDIT_LIMIT = "credit_limit";
    public static final String DISTRIBUTOR_AUTH_STATUS = "auth_status";
    public static final String DISTRIBUTOR_PHONE_NUMBER = "phone_number";
    public static final String DISTRIBUTOR_FAX = "fax";
    public static final String DISTRIBUTOR_SHIPPING_ADDRESS_LINE_1 = "ship_add_line_1";
    public static final String DISTRIBUTOR_SHIPPING_ADDRESS_LINE_2 = "ship_add_line_2";
    public static final String DISTRIBUTOR_SHIPPING_ADDRESS_CITY = "ship_add_city";
    public static final String DISTRIBUTOR_SHIPPING_ADDRESS_STATE = "ship_add_state";
    public static final String DISTRIBUTOR_SHIPPING_ADDRESS_COUNTRY = "ship_add_country";
    public static final String DISTRIBUTOR_SHIPPING_ADDRESS_ZIPCODE = "ship_add_zipcode";
    public static final String DISTRIBUTOR_BILLING_ADDRESS_LINE_1 = "bill_add_line_1";
    public static final String DISTRIBUTOR_BILLING_ADDRESS_LINE_2 = "bill_add_line_2";
    public static final String DISTRIBUTOR_BILLING_ADDRESS_CITY = "bill_add_city";
    public static final String DISTRIBUTOR_BILLING_ADDRESS_STATE = "bill_add_state";
    public static final String DISTRIBUTOR_BILLING_ADDRESS_COUNTRY = "bill_add_country";
    public static final String DISTRIBUTOR_BILLING_ADDRESS_ZIPCODE = "bill_add_zipcode";
    
    /*Dealer*/
    public static final String TABLE_DEALER = "Dealer";
    public static final String DEALER_ID = "id";
    public static final String DEALER_EMAIL = "email";
    public static final String DEALER_PASSWORD = "password";
    public static final String DEALER_FIRST_NAME = "f_name";
    public static final String DEALER_LAST_NAME = "l_name";
    public static final String DEALER_IS_DIRECT = "is_direct";
    public static final String DEALER_CREDIT_LIMIT = "credit_limit";
    public static final String DEALER_AUTH_STATUS = "auth_status";
    public static final String DEALER_PHONE_NUMBER = "phone_number";
    public static final String DEALER_FAX = "fax";
    public static final String DEALER_SHIPPING_ADDRESS_LINE_1 = "ship_add_line_1";
    public static final String DEALER_SHIPPING_ADDRESS_LINE_2 = "ship_add_line_2";
    public static final String DEALER_SHIPPING_ADDRESS_CITY = "ship_add_city";
    public static final String DEALER_SHIPPING_ADDRESS_STATE = "ship_add_state";
    public static final String DEALER_SHIPPING_ADDRESS_COUNTRY = "ship_add_country";
    public static final String DEALER_SHIPPING_ADDRESS_ZIPCODE = "ship_add_zipcode";
    public static final String DEALER_BILLING_ADDRESS_LINE_1 = "bill_add_line_1";
    public static final String DEALER_BILLING_ADDRESS_LINE_2 = "bill_add_line_2";
    public static final String DEALER_BILLING_ADDRESS_CITY = "bill_add_city";
    public static final String DEALER_BILLING_ADDRESS_STATE = "bill_add_state";
    public static final String DEALER_BILLING_ADDRESS_COUNTRY = "bill_add_country";
    public static final String DEALER_BILLING_ADDRESS_ZIPCODE = "bill_add_zipcode";


    /*Products*/
    public static final String ID = "id";
    public static final String NAME = "name";

    public static final String TABLE_DOOR_TYPE = "DoorType";

    public static final String TABLE_DOOR_FRAME = "DoorFrame";

    public static final String TABLE_DOOR_WIDTH = "DoorWidth";

    public static final String TABLE_DOOR_WIDTH_DETAILS = "DoorWidthDetails";
    public static final String DOOR_WIDTH_ID = "door_width_id";
    public static final String WIDTH = "width";

    public static final String TABLE_DOOR_HEIGHT = "DoorHeight";
    public static final String TABLE_DOOR_HEIGHT_DETAILS = "DoorHeightDetails";
    public static final String TABLE_DOOR_PANEL_TYPE = "DoorPanelType";
    public static final String TABLE_DOOR_HANDLING = "DoorHandling";
    public static final String TABLE_DOOR_HANDLING_DETAILS = "DoorHandlingDetails";
    public static final String TABLE_DOOR_ASSEMBLY = "DoorAssembly";
    public static final String TABLE_DOOR_ASSEMBLY_DETAILS = "DoorAssemblyDetails";
    public static final String TABLE_DOOR_FINISH_UNIT = "DoorFinishUnit";
    public static final String TABLE_DOOR_FINISH_UNIT_DETAILS = "DoorFinishUnitDetails";
    public static final String TABLE_DOOR_GLASS_DESIGN = "DoorGlassDesign";
    public static final String TABLE_DOOR_GLASS_DESIGN_DETAILS = "DoorGlassDesignDetails";
    public static final String TABLE_DOOR_GLASS_TYPE = "DoorGlassType";
    public static final String TABLE_DOOR_GLASS_COATING = "DoorGlassCoating";
    public static final String TABLE_DOOR_GLASS_COATING_DETAILS = "DoorGlassCoatingDetails";
    public static final String TABLE_DOOR_GLASS_THICKNESS = "DoorGlassThickness";
    public static final String TABLE_DOOR_HANDLE = "DoorHandle";
    public static final String TABLE_DOOR_LOCK = "DoorLock";
    public static final String TABLE_DOOR_LOCK_DETAILS = "DoorLockDetails";
    public static final String TABLE_DOOR_HANDLE_COLOR = "DoorHandleColor";
    public static final String TABLE_DOOR_HANDLE_COLOR_DETAILS = "DoorHandleColorDetails";

    public static final String TABLE_PREDRILL = "DoorPredrill";
    public static final String PREDRILL_IS_REQUIRED = "is_required";


    public static final String TABLE_DOOR_FRAME_THICKNESS = "DoorFrameThickness";
    public static final String TABLE_DOOR_FRAME_THICKNESS_DETAILS = "DoorFrameThicknessDetails";
}
