package com.gracecomp.retailmanagement;

import com.gracecomp.retailmanagement.bookdemo.Book;
import com.gracecomp.retailmanagement.bookdemo.BookCategory;
import com.gracecomp.retailmanagement.bookdemo.BookCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetailManagementApplication {

    @Autowired
    private static BookCategoryRepository bookCategoryRepository;

    public static void main(String[] args) {
        SpringApplication.run(RetailManagementApplication.class, args);

        bookCategoryRepository.save(new BookCategory("Category 1", new Book("Hello Koding 1"), new Book("Hello Koding 2")));
    }
}
