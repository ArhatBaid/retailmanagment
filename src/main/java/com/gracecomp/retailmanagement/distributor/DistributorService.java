package com.gracecomp.retailmanagement.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public class DistributorService {

    public interface IDistributorService {

        List<Distributor> findAll();
    }

    @Service
    public static class Services implements IDistributorService {

        private List<Distributor> listDistributor = new ArrayList<>();

        @Autowired
        private DistributorRepo distributorRepo;

        @Override
        public List<Distributor> findAll() {
            List<Distributor> listDistributor = new ArrayList<>();
            distributorRepo.findAll().forEach(listDistributor::add);
            return listDistributor;
        }
    }
}

