package com.gracecomp.retailmanagement.distributor;

import com.gracecomp.retailmanagement.dealer.Dealer;
import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_DISTRIBUTOR)
public class Distributor {

    @OneToMany
    private Set<Dealer> inDirectDealers;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.DISTRIBUTOR_ID, nullable = false)
    private Long id;

    @Column(name = DatabaseUtils.DISTRIBUTOR_EMAIL, nullable = false)
    private String email;

    @Column(name = DatabaseUtils.DISTRIBUTOR_PASSWORD, nullable = false)
    private String password;

    @Column(name = DatabaseUtils.DISTRIBUTOR_FIRST_NAME, nullable = false)
    private String f_name;

    @Column(name = DatabaseUtils.DISTRIBUTOR_LAST_NAME, nullable = false)
    private String l_name;

    @Column(name = DatabaseUtils.DISTRIBUTOR_CREDIT_LIMIT, nullable = false)
    private String credit_limit;

    @Column(name = DatabaseUtils.DISTRIBUTOR_AUTH_STATUS, nullable = false)
    private String auth_status;

    @Column(name = DatabaseUtils.DISTRIBUTOR_PHONE_NUMBER, nullable = false)
    private String phone_number;

    @Column(name = DatabaseUtils.DISTRIBUTOR_FAX)
    private String fax;

    @Column(name = DatabaseUtils.DISTRIBUTOR_SHIPPING_ADDRESS_LINE_1, nullable = false)
    private String ship_add_line_1;

    @Column(name = DatabaseUtils.DISTRIBUTOR_SHIPPING_ADDRESS_LINE_2)
    private String ship_add_line_2;

    @Column(name = DatabaseUtils.DISTRIBUTOR_SHIPPING_ADDRESS_CITY, nullable = false)
    private String ship_add_city;

    @Column(name = DatabaseUtils.DISTRIBUTOR_SHIPPING_ADDRESS_STATE, nullable = false)
    private String ship_add_state;

    @Column(name = DatabaseUtils.DISTRIBUTOR_SHIPPING_ADDRESS_COUNTRY, nullable = false)
    private String ship_add_country;

    @Column(name = DatabaseUtils.DISTRIBUTOR_SHIPPING_ADDRESS_ZIPCODE, nullable = false)
    private String ship_add_zipcode;

    @Column(name = DatabaseUtils.DISTRIBUTOR_BILLING_ADDRESS_LINE_1, nullable = false)
    private String bill_add_line_1;

    @Column(name = DatabaseUtils.DISTRIBUTOR_BILLING_ADDRESS_LINE_2)
    private String bill_add_line_2;

    @Column(name = DatabaseUtils.DISTRIBUTOR_BILLING_ADDRESS_CITY, nullable = false)
    private String bill_add_city;

    @Column(name = DatabaseUtils.DISTRIBUTOR_BILLING_ADDRESS_STATE, nullable = false)
    private String bill_add_state;

    @Column(name = DatabaseUtils.DISTRIBUTOR_BILLING_ADDRESS_COUNTRY, nullable = false)
    private String bill_add_country;

    @Column(name = DatabaseUtils.DISTRIBUTOR_BILLING_ADDRESS_ZIPCODE, nullable = false)
    private String bill_add_zipcode;

}
