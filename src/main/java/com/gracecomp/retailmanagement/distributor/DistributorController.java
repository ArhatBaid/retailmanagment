package com.gracecomp.retailmanagement.distributor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

public class DistributorController {

    @Autowired
    private DistributorService.Services distributorService;

    @RequestMapping("/distributor")
    public List<Distributor> getAllDistributors() {
        return distributorService.findAll();
    }
}
