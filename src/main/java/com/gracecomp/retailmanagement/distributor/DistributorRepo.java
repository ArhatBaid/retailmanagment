package com.gracecomp.retailmanagement.distributor;

import org.springframework.data.repository.CrudRepository;

public interface DistributorRepo extends CrudRepository<Distributor, Long> {

}
