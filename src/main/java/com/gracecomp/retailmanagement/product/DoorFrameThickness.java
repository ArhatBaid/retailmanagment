package com.gracecomp.retailmanagement.product;

import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_DOOR_FRAME_THICKNESS)
public class DoorFrameThickness {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.ID, nullable = false)
    private Long id;
}
