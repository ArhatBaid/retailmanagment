package com.gracecomp.retailmanagement.product;

import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_PREDRILL)
public class DoorPredrill {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.ID, nullable = false)
    private Long id;

    @Column(name = DatabaseUtils.PREDRILL_IS_REQUIRED, nullable = false)
    private Boolean is_required;
}
