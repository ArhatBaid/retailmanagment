package com.gracecomp.retailmanagement.product.door_width;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

public class DoorWidthController {

    @Autowired
    private DoorWidthRepo doorWidthRepo;

    @GetMapping("/doorWidth")
    public List<DoorWidthDetails> getAllWidthsById(Long id){
        return doorWidthRepo.findDoorWidthDetailsById(id);
    }
}
