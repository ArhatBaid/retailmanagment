package com.gracecomp.retailmanagement.product.door_width;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_DOOR_WIDTH)
public class DoorWidthDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.ID, nullable = false)
    private Long id;

    @Column(name = DatabaseUtils.DOOR_WIDTH_ID, nullable = false, insertable = false, updatable = false)
    private Integer door_width_id;

    @Column(name = DatabaseUtils.WIDTH, nullable = false)
    private Float width;

    //TODO need to check
    /*@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = DatabaseUtils.DOOR_WIDTH_ID, nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private DoorWidth doorWidth;*/
}
