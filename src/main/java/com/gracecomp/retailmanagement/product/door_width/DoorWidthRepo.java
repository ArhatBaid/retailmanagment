package com.gracecomp.retailmanagement.product.door_width;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
interface DoorWidthRepo extends CrudRepository<DoorWidthDetails, Long> {
    List<DoorWidthDetails> findDoorWidthDetailsById(Long id);
}
