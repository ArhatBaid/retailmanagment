package com.gracecomp.retailmanagement.product.door_width;

import com.gracecomp.retailmanagement.utils.DatabaseUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Getter
@Setter
@Entity
@ToString
@Table(name = DatabaseUtils.TABLE_DOOR_WIDTH)
public class DoorWidth {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = DatabaseUtils.ID, nullable = false)
    private Long id;
}
